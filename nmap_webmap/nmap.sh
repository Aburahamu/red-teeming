#!/usr/bin/env bash
#-
#- Usage:
#-  Manual mode: 
#-  $ ./nmap.sh 
#-  CLI mode: 
#-  $ ./nmap.sh -i {ip/mask} -m {ping|tcp|udp} -d {dir}
#- Options:
#-  -i: ip address. Dose not support hostname  
#-  -m: Mode
#-  -d: Store Directory 
#-  -h: Show this message.  #- #- Example $ ./nmap.sh -c 10.1.1.1/24 -m ping -d /tmp
#-         $ sudo -E ./nmap.sh -c 10.1.1.1/24 -m tcp 


# set -eu #結局nmap自体はエラーになっても継続したい場合あるのでコメント化
# set -o pipefail
cd `dirname $0` || exit 1
# ENV
#export AWS_CONFIG_FILE=${XXX}/.aws/config

# set -x



initlTcpPort="21,22,23,25,53,80,88,110,111,161,389,443,445,465,513,587,636,989,990,993,1433,1521,3389,5900,5985,8222,8333"
#initlUdpPort="6,53,67,69,111,123,135,137,138,161,162,500,514,520"
initlUdpPort="53,67,69,111,123,135,137,138,161,162"

_disp_help(){
    grep "^#-" "$0" | cut -c 4-
}

_check_ip(){
    # https://regex101.com/
    IP_REGEX="^([12])?([0-9])?[0-9]\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(/([0-8]|[12]?[0-9]|[3][0-2]))?$"

    if [[ ! "$1" =~ $IP_REGEX ]] ; then
        echo "\\033[38;5;202m [ - ] Incorrect formats -> $1"
        return 1
    fi
}

ping_scan(){
    # Check valid directory
    ls $2 > /dev/null #set -e だとここで止まってくれる
    [ -d "$2" ] || exit 1; #For set is not 'set -e'

    echo $2`date "+%Y%m%d_%H%M%S"`_${1}_PingScan.txt

    if [[ `echo ${1} | grep -o /` ]]; then
        logdir=$2`date "+%Y%m%d_%H%M%S"`_${1%%/*}_${1#*/}_PingScan
    else
        logdir=$2`date "+%Y%m%d_%H%M%S"`_${1}_PingScan
    fi

    nmap -sn -v --scan-delay 1 --max-rtt-timeout 0.1 --max-retries 1 -oA $logdir $1 #| tee $logdir
    # nmap -sn -v $1 | tee $2`date "+%Y%m%d_%H%M%S"`_${1%%/*}_PingScan.txt
    # docker run --rm nmapimage -sn -v $1 | tee `date "+%Y%m%d_%H%M%S"`_${1%%/*}_PingScan.txt
}

tcp_scan(){
    # Check valid directory
    ls $2 > /dev/null
    [ -d "$2" ] || exit 1; #For set is not 'set -e'

    # echo Confirm target ip to $1  OK? You must specific localhost only!  #For debug
    # read _ #For debug

    #init scan #sudo -E nmap -sS -p 23,22,80,443,445 -T3 -A -sV -O -v -v -Pn --scan-delay 0.1 --max-rtt-timeout 0.1 --max-retries 1 --host-timeout 7000s \
    # sudo -E nmap -sS -p $initlTcpPort -T4 -A -sV -O -v -v -Pn --max-rtt-timeout 0.1 --max-retries 1 --host-timeout 7000s \
    # -oA $2`date "+%Y%m%d_%H%M%S"`_${1%%/*}_${1#*/}_BasicScan $1 \
    sudo -E nmap -sT -p $initlTcpPort -sV -O -Pn --max-rtt-timeout 0.1 --max-retries 1 --host-timeout 7000s \
    -oA $2`date "+%Y%m%d_%H%M%S"`_${1%%/*}_${1#*/}_BasicScan $1 \

    # いくつかのプロトコルで疎通確認して全ポート
    # nmap -PE -PP -PS22,23,80,443,445 -p- --scan-delay 1.0 --max-rtt-timeout 0.1 --max-retries 1 --host-timeout 7000s -oN {filename} --min-hostgroup 64 10.55.2.0/23

    #強制全ポート
    # sudo -E nmap -sS -p- -T3 -A -sV -O -v -v -Pn -oN $2`date "+%Y%m%d_%H%M%S"`_${1%%/*}_BasicScan.txt \

    # docker run --rm --privileged nmapimage -sS -p 23,80,443,445 -T3 -A -sV -O -v -v -Pn -oN `date "+%Y%m%d_%H%M%S"`_${1%%/*}_BasicScan.txt \
    #         nmap -sS -p- -T3 -A -sV -O -v -v -Pn -oN `date "+%Y%m%d_%H%M%S"`_${1%%/*}_BasicScan.txt \
}

#その他サンプル
#  nmap -sn -v ${nw} --scan-delay 10 --max-rtt-timeout 0.1 --max-retries 1
#  nmap -PE -PP -PS22,23,80,443,445 -p- --scan-delay 0.1 --max-rtt-timeout 0.1 --max-retries 1 --host-timeout 7000s -oN {filename} --min-hostgroup 64 10.55.2.0/23
#log取得系はbatスクリプトの方で
#  tshark -i 1 -w 20190527.pcap -b filesize:100000 -f 'host 54.xxx.xxx.xxx or host 172.23.0.1' &
#  sudo tcpdump -i eth0 dst net 10.0.0.0/21 or net  -w alive_check.pcap -C 30

#init scan
#nmap -sV -O --top-ports 50 --open -oA nmap/initial <ip or cidr>
#full
#nmap -sC -sV -O --open -p- -oA nmap/full <ip or cidr>


udp_scan(){
    # Check valid directory
    ls $2 > /dev/null
    [ -d "$2" ] || exit 1; #For set is not 'set -e'
    nmap -sU -p $initlUdpPort -oA $2`date "+%Y%m%d_%H%M%S"`_${1%%/*}_${1#*/}_udpScan $1
}

_manual_chk_ip() {
    _check_ip $1
    if [ $? -ne 0 ]; then
        echo Press ENTER to return to menu
        continue;
    fi
    echo $1
}

_make_result_dir (){
    mkdir -p $1/result
    echo "$1/result/" 
}

# _check_ip で引っかかると呼ばれるようだ。
# trap 'echo "`date "+%Y-%m-%d:%H:%M:%S"` Something failed. trap nmap 1 $ip" >> $faillog' ERR 
trap 'echo "`date "+%Y-%m-%d:%H:%M:%S"` Something failed. trap nmap 1 $ip" >&2' ERR 

function manual() {
    clear
    result_dir=`_make_result_dir $1`
    select opt in "ping_scan" "tcp_scan" "udp_scan"
    do
        if [ $opt = "ping_scan" ]; then
            echo "Specify target ip address  ex.) 192.168.100.1/32"
            read ip
            _manual_chk_ip $ip 
            ping_scan $ip $result_dir
            break;
        elif [ $opt = "tcp_scan" ]; then
            echo "Specify target ip address  ex.) 192.168.100.1/32"
            read ip
            _manual_chk_ip $ip
            tcp_scan $ip $result_dir
            break;
        elif [ $opt = "udp_scan" ]; then
            # _manual_chk_ip
            echo "Specify target ip address  ex.) 192.168.100.1/32"
            read ip
            _manual_chk_ip $ip
            udp_scan $ip $result_dir
            # echo ping_scan $ip
            break;
        else
            echo "Please press 1) or 2)";
        fi
    done
    exit 0
}


# Manual mode
[ $# -le 0 ] && manual './'


# CLI mode
while getopts i:m:d:h OPT
do
    case "$OPT" in
        i )
            ip=$OPTARG
            _check_ip $ip
            [ $? -eq 0 ] || exit 1; #For set is not 'set -e'
            ;;
        m )
            case "$OPTARG" in
                ping)
                    exec_nmap=ping_scan ;;
                tcp)
                    exec_nmap=tcp_scan ;;
                udp)
                    exec_nmap=udp_scan ;;
                * ) 
                    echo Undifined mode; exit 0;;
            esac
            ;;
        d )
            store_dir=${OPTARG}/
            ;;
        h | * )
            _disp_help; exit 0
            ;;
        # \?) echo "[ERROR] Undefined command."
        #     _disp_help
        #     exit 0
    esac
done

# 必須パラメタ確認。パラメータ展開を利用の未定義確認
if [ -z "${ip+UNDEF}" ] || [ -z "${exec_nmap+UNDEF}" ];then
    _disp_help; exit 0
fi

# set -x
# $store_dir はデフォルト値はカレントdir
result_dir=$(_make_result_dir ${store_dir-./})

$exec_nmap $ip $result_dir 

# 毎回呼ばれてるようだ
# trap 'echo `date "+%Y-%m-%d:%H:%M:%S"` What trap nmap 3 $ip >> nmap-error.log' EXIT
