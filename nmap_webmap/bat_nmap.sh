#!/bin/bash
set -eu #loop前など、0判定でstopしてしまう所は適宜解除
set -o pipefail


if [[ "$#" -lt 2 ]];then
    cat <<EOF
    Usage: ./bat_nmap.sh <file> <mode> <option:dir(default:/tmp/webmap)>
    Example:
    $ ./bat_nmap.sh ip.lst ping ./20190701
    If sleepswitch.stop exists, stop process.
EOF
     exit 0
fi 

# logdir="./"
logdir=${3-/tmp/webmap}
faillog="error.log"
mode=$2

if [[ "$mode" != "ping" ]] && [[ "$mode" != "tcp" ]];then
    echo "Unsupported scan mode -> $mode"; exit 1
fi

_start_tcpdump (){
    mkdir -p ./tcpdump
    sudo tcpdump -C 50 -w ./tcpdump/log_`date +%Y%m%d-%H%M%S`.pcap &
    #PID的なの取得して終わるときにkillする　trapとかかな
}

_check_install_nmap () {
    echo "[ + ] Checking install nmap version . . ."
    # nmap -v > /dev/null 2&>1
    echo $(nmap -v | head -n 1 | grep -e 'Nmap [0-9]*\.[0-9]*' -o)
    if [ $? -ne 0 ]; then
        echo "[ - ] Nmap dose not exist.." 
        exit 1
    fi
}

_init_webmap () {
    image=$(docker images | grep -e 'reborntc/webmap\s'| wc -l|tr -d ' ') || true
    [ "$image" -eq 0 ] && ( echo "[ + ] Pulling docker image..."; sudo docker pull reborntc/webmap )

    # [ -n "$container" ] && ( echo "[ + ] Docker container regenerating..."; sudo docker stop $container; sudo docker rm $container )

    container=`docker ps -a | grep 'reborntc/webmap\s' | awk '{print $1}' | tr -d ' '` || true
    mkdir -m 755 $logdir || true

    if [[ -z "$container" ]];then
    echo "[ + ] Docker container generating..."
    sudo docker run -d \
        --name webmap \
        -h webmap \
        -p 8000:8000 \
        -v $logdir/result:/opt/xml \
        reborntc/webmap
    fi
}

sleepswitch() {
    while [ -e ./sleepswitch.stop ]
    do
        sleep 10
        echo -n .
    done
}

_parse_pnigscan_res(){
    echo "[ + ] Parsing result files... from $logdir/result"; sleep 2
    ./parse_ping_scan.sh $logdir/result | tee `date "+%Y-%m-%d:%H:%M"`_alived.lst
    echo "[ + ] Output to `date "+%Y-%m-%d:%H:%M"`_alived.lst"
}


_check_install_nmap
_init_webmap


# エラー時のlogを出力してあとでどこまで終わってるか確認できるようにする trap使えばできる？ or inputファイルとnmapログを突合させる？？
# 不正なIPを途中で入れて挙動をみる。そのIPだけSKIPされ処理が止まらず、logで確認できること
# while read ip
# do
# done < $1
set +e  #エラー停止を解除
for ip in `cat $1 | grep -v '^#'`
do
    echo "--- start $mode scan for $ip ---"
    sleepswitch
    ./nmap.sh -i $ip -m $mode -d $logdir 2>> $faillog
    echo "`date "+%Y-%m-%d:%H:%M:%S"` Compeleted nmap.sh: $ip" >> complete.log
done

# 処理が終わった時に呼ばれるようだ
# trap "rm -f webmap_token.txt" EXIT
# trap "echo `date "+%Y-%m-%d:%H:%M:%S"` TTtrap From auto_nmap.sh >> $faillog" EXIT

if [[ -e webmap_token.txt ]];then
    echo "`cat webmap_token.txt`"
else
    echo "$(docker exec -ti webmap /root/token)" | tee webmap_token.txt
fi

echo "[ + ] Please access to http://localhost:8000/ !!\n"


# When ping scan
[[ "$mode" == "ping" ]] && _parse_pnigscan_res;
