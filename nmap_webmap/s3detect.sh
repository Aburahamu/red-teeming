#!/bin/bash

usage(){
cat <<EEE
 Usage:
  cat <host.list> | s3detect.sh
  Generate file to s3.list
EEE
}

set -u

#if [[ $# -ne 0 ]];then
if [ ! -p /dev/stdin ];then
 usage;
 exit 1
fi


IP_REGEX="^([12])?([0-9])?[0-9]\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(/([0-8]|[12]?[0-9]|[3][0-2]))?$"

while read host
do
 [[ "$host" =~ $IP_REGEX ]] && continue;
 echo $host
 s3=`dig $host | egrep -i -o 'IN\sCNAME\ss3-.*' | sed -E 's/.*CNAME +//g'`
 
 sleep 1
 [[ -z $s3 ]] && continue;
 
 echo "$host	$s3" | tee -a s3.list
done #< $1
