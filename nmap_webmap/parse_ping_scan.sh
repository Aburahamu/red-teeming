#!/bin/bash
set -o pipefail

if [[ $# -eq 0 ]];then
  echo "Usage: $ $0 <Pingscan logs dir> "
  exit 0
fi

logdir=${1:-./result}

cat $logdir/*PingScan.nmap | grep -v '\[host down\]' | \
grep '^Nmap scan report' | \
awk '{if($5 ~ /[a-zA-Z]$/){ print $6 }else{ print $5 }}' | \
tr -d '()' | \
sort | \
uniq