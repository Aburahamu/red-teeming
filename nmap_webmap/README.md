

## Init
At the first time of `loop_nmap.sh`, the webmap image is downloaded and the container is created.
### Create IP list
```
cat << EOF > ip.list
127.0.0.1/32
#1.1.1.1
weak.example.com
127.0.0.9
EOF
```
## Descovery
### Ping scan
```
./loop_nmap.sh ip.list ping
```
By default, stored logfile in `/tmp/webamp/result`
```
tree /tmp/webmap/result
/tmp/webmap/result
├── 20200514_224047_127.0.0.1_PingScan.txt
├── 20200514_224048_127.0.0.9_PingScan.txt
└── 20200514_230931_127.0.0.1_PingScan.txt
```

### Parse ping scan logs
```
./parse_ping_scan.sh /tmp/webmap/result
[ + ] Parsing result files... from /tmp/webmap/result
127.0.0.1
[ + ] Output to alived.list
```

## Excute some scan
### TCP scan
```
/loop_nmap.sh alived.list tcp
[ + ] Checking install nmap version . . .
WARNING: No targets were specified, so 0 hosts scanned.
Nmap 7.80
mkdir: /tmp/webmap: File exists
--- start tcp scan for 127.0.0.1 ---
```
By default, stored logfile in /tmp/webamp/result
```
tree /tmp/webmap/result
/tmp/webmap/result
├── 20200514_224047_127.0.0.1_PingScan.txt
├── 20200514_224048_127.0.0.9_PingScan.txt
├── 20200514_230931_127.0.0.1_PingScan.txt
├── 20200514_232201_127.0.0.1_BasicScan.txt
└── 20200514_232201_127.0.0.1_BasicScan.xml
```
## Access to dashboad
```
http://localhost:8000/
```

If there are servers that could be not answering (ping), then add the flag -Pn (example of initial one):

```
nmap -Pn --top-ports 50 --open -oA nmap/initial <ip or cidr>
```

-----------
## Todo
Write test ...
Fetch API ..
Exec NSE script
