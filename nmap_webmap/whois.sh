#!/bin/bash
# 検索元の一覧のIPの中に特定のドメインが含まれるかどうか調べる
# Usage:
# ./whois.sh {search word} < dns_ip

domain=${1:-yahoo}
reg="netname.*$domain"

echo Search for $domain ...

while read host xxx ip 
do
echo $ip;
res=`whois $ip | grep -i -o $reg` > /dev/null
 if [[ $? -eq 0 ]];then
   echo $host $ip | tee -a is_$domain.log
 else
   echo Another Org... $host $ip $res 1>&2
 fi
sleep 0.51
done 

