#!/bin/bash
# 検索元の一覧のhostのcnameレコードを検索する

# cat is_cname.log| awk '{if($5 == "alias"){print $0}else{print $1"  "$5}}'
# ^(.*?)\s
# $1\t

if [ ! -p /dev/stdin ];then
cat <<EOF
Usage:
cat host.list | ./cname.sh
EOF
 exit 1
fi



while read host xxx ip 
do
echo Search for $host ...
res=`host $host 8.8.8.8 | egrep -io -A 18 'alias\sfor\s+.*'` > /dev/null
#res=`dig cname $host @8.8.8.8 | egrep -i -o -A 18 'IN\s+cname[^\s]+'` > /dev/null
 if [[ $? -eq 0 ]];then
   echo $host $res | tee -a is_cname.log
 else
   echo No detect CNAME record... $host $res 1>&2
 fi
sleep 1
done 

